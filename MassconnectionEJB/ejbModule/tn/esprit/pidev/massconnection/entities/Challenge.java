package tn.esprit.pidev.massconnection.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Challenge
 * 
 */
@Entity
public class Challenge implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private Details details;
	private Boolean validationStatus;
	private Crowd crowd;
	/*
	 * attribute with system date when creation of challenge for generating
	 * statistics about number of project per day , month and year
	 */
	private Date creationDate;
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = new Date();
	}

	public Boolean getValidationStatus() {
		return validationStatus;
	}

	private List<Participation> participations;

	@OneToMany(mappedBy = "challenge")
	public List<Participation> getParticipations() {
		return participations;
	}

	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}

	@ManyToOne
	public Crowd getCrowd() {
		return crowd;
	}

	public void setCrowd(Crowd crowd) {
		this.crowd = crowd;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Embedded
	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	@Column(name = "validationstatus", columnDefinition = "bit")
	public Boolean isValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(Boolean validationStatus) {
		this.validationStatus = validationStatus;
	}

	public Challenge() {
		super();
	}

}
