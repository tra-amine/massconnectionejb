package tn.esprit.pidev.massconnection.servicesinterface;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;



import tn.esprit.pidev.massconnection.entities.Crowd;

@Local
public interface CrowdCrudLocal {
	public List<Crowd> findAll();

	public void create(Crowd crowd);

	public void edit(Crowd crowd);

	public void remove(Crowd crowd);

	public Crowd find(int category);
	/**
	 * 
	 * @return a map with key=type of crowd and value=percent
	 */
	public Map<String, Long> statisticsTypesOfCrowds();
}
