package tn.esprit.pidev.massconnection.servicesinterface;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.massconnection.entities.Project;

@Remote
public interface ProjectCrudRemote {
	public List<Project> findAll();

	public void create(Project project);

	public void edit(Project project);

	public void remove(Project project);

	public Project find(int project);
}
