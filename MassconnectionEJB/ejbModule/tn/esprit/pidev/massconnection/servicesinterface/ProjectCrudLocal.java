package tn.esprit.pidev.massconnection.servicesinterface;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.massconnection.entities.Project;

@Local
public interface ProjectCrudLocal {
	public List<Project> findAll();

	public void create(Project project);

	public void edit(Project project);

	public void remove(Project project);

	public Project find(int project);
}
