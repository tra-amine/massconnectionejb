package tn.esprit.pidev.massconnection.servicesinterface;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.massconnection.entities.Solution;

@Remote
public interface SolutionCrudRemote {
	public List<Solution> findAll();

	public void create(Solution solution);

	public void edit(Solution solution);

	public void remove(Solution solution);

	public Solution find(int solution);
}
