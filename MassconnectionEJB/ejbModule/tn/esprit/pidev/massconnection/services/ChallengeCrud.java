package tn.esprit.pidev.massconnection.services;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pidev.massconnection.entities.Challenge;
import tn.esprit.pidev.massconnection.servicesinterface.ChallengeCrudLocal;
import tn.esprit.pidev.massconnection.servicesinterface.ChallengeCrudRemote;

/**
 * Session Bean implementation class ChallengeCrud
 */
@Stateless

public class ChallengeCrud extends AbstractFacade<Challenge>implements ChallengeCrudRemote, ChallengeCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
    private EntityManager em;
    public ChallengeCrud() {
       super(Challenge.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
