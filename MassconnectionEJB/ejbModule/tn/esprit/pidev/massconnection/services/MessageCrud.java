package tn.esprit.pidev.massconnection.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pidev.massconnection.entities.Message;
import tn.esprit.pidev.massconnection.servicesinterface.MessageCrudRemote;
import tn.esprit.pidev.massconnection.servicesinterface.MessageCrudLocal;

/**
 * Session Bean implementation class MessageCrud
 */
@Stateless
public class MessageCrud extends AbstractFacade<Message>implements MessageCrudRemote, MessageCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
    private EntityManager em;
    public MessageCrud() {
       super(Message.class);
    }
	@Override
	protected EntityManager getEntityManager() {
		
		return em;
	}

}
