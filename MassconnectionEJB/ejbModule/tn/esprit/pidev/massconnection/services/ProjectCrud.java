package tn.esprit.pidev.massconnection.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pidev.massconnection.entities.Project;
import tn.esprit.pidev.massconnection.servicesinterface.ProjectCrudLocal;
import tn.esprit.pidev.massconnection.servicesinterface.ProjectCrudRemote;

/**
 * Session Bean implementation class ProjectCrud
 */
@Stateless
public class ProjectCrud extends AbstractFacade<Project>implements ProjectCrudRemote, ProjectCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
    private EntityManager em;
    public ProjectCrud() {
        super(Project.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em ;
	}

}
