package tn.esprit.pidev.massconnection.services;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pidev.massconnection.entities.Category;
import tn.esprit.pidev.massconnection.servicesinterface.CategoryCrudLocal;
import tn.esprit.pidev.massconnection.servicesinterface.CategoryCrudRemote;

/**
 * Session Bean implementation class CategoryCrud
 */
@Stateless

public class CategoryCrud extends AbstractFacade<Category> implements CategoryCrudRemote, CategoryCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
    private EntityManager em;
    public CategoryCrud() {
        super(Category.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	

}
