package tn.esprit.pidev.massconnection.services;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.pidev.massconnection.entities.Administrator;
import tn.esprit.pidev.massconnection.entities.Crowd;
import tn.esprit.pidev.massconnection.servicesinterface.CrowdCrudLocal;
import tn.esprit.pidev.massconnection.servicesinterface.CrowdCrudRemote;

/**
 * Session Bean implementation class CrowdCrud
 */
@Stateless
public class CrowdCrud extends AbstractFacade<Crowd> implements
		CrowdCrudRemote, CrowdCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
	private EntityManager em;

	public CrowdCrud() {
		super(Crowd.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Map<String, Long> statisticsTypesOfCrowds() {
		Long nbre;
		Query query;
		Map<String, Long> stat = new HashMap<String, Long>();

		query = em.createNamedQuery("Crowd.statisticsCrowdsCreator");

		nbre =   (Long) query.getSingleResult();
		stat.put("Creator", nbre);

		query = em.createNamedQuery("Crowd.statisticsCrowdsInvestor");

		nbre =   (Long) query.getSingleResult();
		stat.put("Investor", nbre);
		query = em.createNamedQuery("Crowd.statisticsCrowdsChallengeCreator");
		nbre =   (Long) query.getSingleResult();
		stat.put("Challenge Creator", nbre);

		query = em.createNamedQuery("Crowd.statisticsCrowdsChallengeSolver");

		nbre =   (Long) query.getSingleResult();
		stat.put("Challenge Solver", nbre);
		return stat;

	}

}
