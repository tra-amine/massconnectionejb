package tn.esprit.pidev.massconnection.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pidev.massconnection.entities.Solution;
import tn.esprit.pidev.massconnection.servicesinterface.SolutionCrudLocal;
import tn.esprit.pidev.massconnection.servicesinterface.SolutionCrudRemote;

/**
 * Session Bean implementation class SolutionCrud
 */
@Stateless
public class SolutionCrud extends AbstractFacade<Solution> implements SolutionCrudRemote, SolutionCrudLocal {

	@PersistenceContext(unitName = "MassconnectionEJB")
    private EntityManager em;
    public SolutionCrud() {
       super(Solution.class);
    }
       
 
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
